package allesio.daofx.jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;

public class DBConnector {
	private Connection connection;
	private DatabaseType databaseType;

	private String url = "";
	private String upoIme = "";
	private String upoGeslo = "";
	private boolean keepAlive = true;

	public enum DatabaseType {
		POSTGRESQL("org.postgresql.Driver", "jdbc:postgresql:");
		// TODO add others ...

		private final String driverString;
		private final String jdbcString;

		private DatabaseType(String driverString, String jdbcString) {
			this.driverString = driverString;
			this.jdbcString = jdbcString;
		}

		public String getDriverString() {
			return this.driverString;
		}

		public String getJDBCConnectionString() {
			return jdbcString;
		}
	}

	public DBConnector(String url, String schemaName, String user, String pass)
			throws SQLException, ClassNotFoundException {
		this.databaseType = DatabaseType.POSTGRESQL;
		this.url = databaseType.getJDBCConnectionString() + "//" + url + "/" + schemaName;
		this.upoIme = user;
		this.upoGeslo = pass;
		openConnection(true);
	}

	public DBConnector(String url, String user, String pass) throws SQLException, ClassNotFoundException {
		this(url, user, pass, true, DatabaseType.POSTGRESQL);
	}

	public DBConnector(String url, String user, String pass, boolean keepAlive, DatabaseType databaseType)
			throws SQLException, ClassNotFoundException {
		this.databaseType = databaseType;
		if (!url.startsWith(databaseType.getJDBCConnectionString())) {
			url = databaseType.getJDBCConnectionString() + "//" + url;
		}
		this.url = url;
		this.upoIme = user;
		this.upoGeslo = pass;
		openConnection(keepAlive);
	}

	public void openConnection(boolean keepAlive) throws SQLException, ClassNotFoundException {
		if (connection != null && !connection.isClosed())
			return;
		checkClass(databaseType.getDriverString());
		this.keepAlive = keepAlive;
		Properties props = new Properties();
		props.setProperty("user", upoIme);
		props.setProperty("password", upoGeslo);
		props.setProperty("tcpKeepAlive", String.valueOf(keepAlive));
		connection = DriverManager.getConnection(url, props);

	}

	private Class<?> checkClass(String name) throws ClassNotFoundException {
		return Class.forName(name);
	}

	public void closeConnection() throws SQLException {
		if (connection != null && !connection.isClosed()) {
			connection.close();
		}
	}

	public void connectionNULL() throws SQLException {
		closeConnection();
		connection = null;
	}

	public Connection getConnection() {
		return connection;
	}

	public void runStatement(String queryString) throws ClassNotFoundException, SQLException {
		Statement stmt = null;
		if (connection == null || connection.isClosed())
			openConnection(true);
		stmt = connection.createStatement();
		stmt.execute(queryString);
		stmt.close();
	}

	public ResultSet select(String queryString) throws SQLException, ClassNotFoundException {
		if (connection == null || connection.isClosed())
			openConnection(true);
		Statement stmt = connection.createStatement();
		ResultSet rs = stmt.executeQuery(queryString);
		return rs;
	}

	public void commit() throws SQLException {
		if (connection != null) {
			connection.commit();
		}
	}

	public void rollback() throws SQLException {
		if (connection != null) {
			connection.rollback();
			connection.setAutoCommit(true);
		}
	}

	public void updateManualCommit(String queryString) throws SQLException, ClassNotFoundException {
		if (connection == null || connection.isClosed())
			openConnection(keepAlive);
		if (connection.getAutoCommit()) {
			connection.setAutoCommit(false);
		}
		Statement st = null;
		st = connection.createStatement();
		st.executeUpdate(queryString);
		st.close();
	}

	public void update(String queryString) throws ClassNotFoundException, SQLException {
		if (connection == null || connection.isClosed())
			openConnection(keepAlive);
		if (!connection.getAutoCommit())
			connection.setAutoCommit(true);

		Statement st = null;
		st = connection.createStatement();
		st.executeUpdate(queryString);
		st.close();
	}

	public int batchUpdate(String... sqlStrings) throws SQLException {
		if (sqlStrings == null || sqlStrings.length == 0)
			return 0;
		Statement statement = null;
		statement = connection.createStatement();

		for (int i = 0; i < sqlStrings.length; i++) {
			// adding batchs to the statement
			statement.addBatch(sqlStrings[i]);
		}
		// usage of the executeBatch method
		int[] recordsUpdated = statement.executeBatch();
		int total = 0;
		for (int recordUpdated : recordsUpdated) {
			total += recordUpdated;
		}
		return total;
	}

	public int batchPrepearedUpdate(String sqlString, Object[][] parameters) throws SQLException {
		if (sqlString == null || sqlString.isEmpty() || parameters == null)
			return 0;
		PreparedStatement statement = null;
		statement = connection.prepareStatement(sqlString);

		for (int i = 0; i < parameters.length; i++) {
			Object[] obj = parameters[i];
			for (int j = 0; j < obj.length; j++) {
				statement.setObject(j + 1, obj[j]);
			}
			statement.addBatch();
		}
		// usage of the executeBatch method
		int[] recordsUpdated = statement.executeBatch();
		int total = 0;
		for (int recordUpdated : recordsUpdated) {
			total += recordUpdated;
		}
		return total;
	}
}
