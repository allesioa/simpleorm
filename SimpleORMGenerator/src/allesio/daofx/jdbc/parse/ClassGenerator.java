package allesio.daofx.jdbc.parse;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;

import allesio.daofx.jdbc.DBConnector;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class ClassGenerator {
	private ObservableList<TableColumnInfo> columns;
	private DBConnector dbConnector;
	private String tableName;
	private String packageName;

	public ClassGenerator(DBConnector dbConnector) {
		this.dbConnector = dbConnector;
	}

	public void build(String tableName) throws ClassNotFoundException, SQLException {
		this.tableName = tableName;
		/*
		 * NOTICE tried SELECT FROM information_schema.columns - its more time
		 * consuming
		 */
		String query = "SELECT * from " + tableName + " LIMIT 0";

		ResultSet results = dbConnector.select(query);
		if (null == results)
			return;

		ResultSetMetaData resultSetMetaData = results.getMetaData();

		columns = FXCollections.observableArrayList();
		for (int i = 1; i <= resultSetMetaData.getColumnCount(); i++) {
			String columnName = resultSetMetaData.getColumnName(i);
			String columnTypeName = resultSetMetaData.getColumnTypeName(i);
			String columnClassName = resultSetMetaData.getColumnClassName(i);
			columns.add(
					new TableColumnInfo(columnName, columnTypeName, fixReturnClass(columnTypeName, columnClassName)));
			// System.out.println(columnName + ", " + columnTypeName + ", " +
			// columnClassName);
		}
		if (null != results.getStatement()) {
			results.getStatement().close();
		}
		results.close();
	}

	/**
	 * If we want to change return class type to something different that we get
	 * from jdbc columnTypeName then we can change it here
	 * 
	 * @param columnTypeName
	 * @param columnClassName
	 * @return
	 */
	private String fixReturnClass(String columnTypeName, String columnClassName) {
		switch (columnTypeName) {
		case "bytea": // it returns [B
			return "byte[]";
		default:
			return columnClassName;
		}
	}

	public ObservableList<TableColumnInfo> getColumns() {
		return columns;
	}

	public String getTableName() {
		return tableName;
	}

	public void setPackageName(String packageName) {
		this.packageName = packageName;
	}

	public String getPackageName() {
		return packageName;
	}
}
