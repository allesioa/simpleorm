package allesio.daofx.jdbc.parse;

import java.sql.SQLException;
import java.util.List;

import allesio.daofx.utils.DBStrings;

public class BuildORM {
	private StringBuilder sb;
	private String packageName;
	private List<TableColumnInfo> columns;
	private String optionalClassName;

	public void build(String tableName, String optionaORMClassName, String optionalClassName,
			List<TableColumnInfo> columns) throws ClassNotFoundException, SQLException {
		this.optionalClassName = optionalClassName;
		if (null == optionalClassName) {
			optionalClassName = DBStrings.capitalize(tableName);
		}

		this.columns = columns;
		sb = new StringBuilder();

		// create java class
		if (null != getPackageName()) {
			sb.append("package " + getPackageName() + ".orm;\n\n");
		}
		sb.append("import java.sql.Connection;\r\n" + "import java.sql.PreparedStatement;\r\n"
				+ "import java.sql.ResultSet;\r\n" + "import java.sql.Statement;\r\n"
				+ "import java.sql.SQLException;\r\n" + "import java.util.List;\r\n" + "import java.util.ArrayList;\r\n"
				+ "import " + getPackageName() + "." + optionalClassName + ";\r\n\r\n");

		sb.append("public class ");
		if (null != optionaORMClassName) {
			sb.append(optionaORMClassName);
		} else {
			sb.append(DBStrings.capitalize(tableName) + "ORM");
		}
		sb.append("{ \n");
		create(tableName);
		update(tableName);
		delete(tableName);
		select(tableName);
		getResults(tableName);

		sb.append("\tprotected void close(Statement stmt) {\r\n" + "        if (stmt != null) {\r\n"
				+ "            try {\r\n" + "                stmt.close();\r\n"
				+ "            }catch(SQLException e){}\r\n" + "        }\r\n" + "    }\n");
		sb.append("\tprotected void close(ResultSet rs) {\r\n" + "        if (rs != null) {\r\n"
				+ "            try {\r\n" + "                rs.close();\r\n"
				+ "            }catch(SQLException e){}\r\n" + "        }\r\n" + "    }");
		sb.append("}");
	}

	private void create(String tableName) {
		sb.append("\tprotected static String SQL_INSERT=\"INSERT INTO " + tableName + "(" + getColumns(true)
				+ ") VALUES (" + getQuestionmarks() + ")\"; \n");
		sb.append("\tpublic void create(" + optionalClassName + " bean, Connection conn) throws SQLException {\n");
		sb.append("\tPreparedStatement ps = null;\r\n" + "\t\ttry {\r\n"
				+ "\t\tps = conn.prepareStatement(SQL_INSERT);\n");
		int index = 1;
		for (int i = 1; i <= columns.size(); i++) {
			TableColumnInfo tableColumnInfo = columns.get(i - 1);
			if (!tableColumnInfo.isInsert())
				continue;
			sb.append("\t\t\tps.set" + getClassType(tableColumnInfo.getColumnClassName()) + "(" + (index++) + ", bean."
					+ DBStrings.toJavaVariable("get_" + tableColumnInfo.getName()) + "());\r\n");
		}
		sb.append("\t\tps.executeUpdate();\n");
		sb.append("\t\t}finally {\r\n" + "\t\t\tclose(ps);\r\n" + "\t\t}\n");
		sb.append("\t}\n");
	}

	private void update(String tableName) {
		sb.append(
				"\tprotected static String SQL_UPDATE=\"UPDATE " + tableName + " SET " + getColumnsUpdate() + "\"; \n");
		sb.append("\tpublic void update(" + optionalClassName + " bean, Connection conn) throws SQLException {\n");
		sb.append("\tPreparedStatement ps = null;\r\n" + "\t\ttry {\r\n"
				+ "\t\tps = conn.prepareStatement(SQL_UPDATE);\n");

		int index = 1;
		for (int i = 1; i <= columns.size(); i++) {
			TableColumnInfo tableColumnInfo = columns.get(i - 1);
			if (!tableColumnInfo.isUpdate())
				continue;
			sb.append("\t\t\tps.set" + getClassType(tableColumnInfo.getColumnClassName()) + "(" + (index++) + ", bean."
					+ DBStrings.toJavaVariable("get_" + tableColumnInfo.getName()) + "());\r\n");
		}
		sb.append("\t\tps.executeUpdate();\n");
		sb.append("\t\t}finally {\r\n" + "\t\t\tclose(ps);\r\n" + "\t\t}\n");
		sb.append("\t}\n");

	}

	private void delete(String tableName) {
		sb.append("\tprotected static String SQL_DELETE=\"DELETE FROM " + tableName + "\"; \n");
		sb.append("\tpublic void delete(" + optionalClassName + " bean, Connection conn) throws SQLException {\n");
		sb.append("\tPreparedStatement ps = null;\r\n" + "\t\ttry {\r\n"
				+ "\t\tps = conn.prepareStatement(SQL_DELETE);\n");
		sb.append("\t\tps.executeUpdate();\n");
		sb.append("\t\t}finally {\r\n" + "\t\t\tclose(ps);\r\n" + "\t\t}\n");
		sb.append("\t}\n");

	}

	private void select(String tableName) {
		sb.append(
				"\tprotected static String SQL_SELECT=\"SELECT " + getColumns(false) + " FROM " + tableName + "\"; \n");
		sb.append("\tpublic List<" + optionalClassName + "> select(Connection conn) throws SQLException {\n");
		sb.append("\tPreparedStatement ps = null;\r\n" + "\tResultSet rs = null;\r\n" + "\t\ttry {\r\n"
				+ "\t\tps = conn.prepareStatement(SQL_SELECT);\n");
		sb.append("\t\trs = ps.executeQuery();\n");
		sb.append("\t\treturn getResults(rs);\n");
		sb.append("\t\t}finally {\r\n" + "\t\t\tclose(rs);\r\n" + "\t\t\tclose(ps);\r\n" + "\t\t}\n");
		sb.append("\t}\n");
	}

	private void getResults(String tableName) {
		sb.append("\tpublic List<" + optionalClassName + "> getResults(ResultSet rs) throws SQLException {\n");

		sb.append("\tList<" + optionalClassName + "> results = new ArrayList<" + optionalClassName + ">();\r\n"
				+ "\t\twhile (rs.next()) {\n");
		sb.append("\t\t" + optionalClassName + " bean = new " + optionalClassName + "();\r\n");
		for (int i = 1; i <= columns.size(); i++) {
			TableColumnInfo tableColumnInfo = columns.get(i - 1);
			if (!tableColumnInfo.isSelect())
				continue;
			sb.append("\t\t\tbean." + DBStrings.toJavaVariable("set_" + tableColumnInfo.getName()) + "(rs.get"
					+ getClassType(tableColumnInfo.getColumnClassName()) + "(\"" + tableColumnInfo.getName() + "\"))"
					+ ";\r\n");
		}
		sb.append("\tresults.add(bean);\r\n" + "\t\t}\r\n" + "\t\treturn results;\r\n" + "    }\n");
	}

	/**
	 * @param columnClassName
	 * @return
	 */
	private String getClassType(String columnClassName) {
		int lastIndexOf = columnClassName.lastIndexOf('.');
		if (lastIndexOf > 0) {
			String substring = columnClassName.substring(++lastIndexOf);
			switch (substring) {
			case "Integer": // jdbc converts to int
				substring = "Int";
				break;
			default:
				break;
			}
			return substring;
		}

		switch (columnClassName) {
		case "byte[]":
			columnClassName = "Bytes";
			break;

		default:
			break;
		}
		return columnClassName;
	}

	private String getQuestionmarks() {
		StringBuilder sb = new StringBuilder();
		boolean first = true;
		for (int i = 0; i < columns.size(); i++) {
			TableColumnInfo tableColumnInfo = columns.get(i);
			if (!tableColumnInfo.isInsert())
				continue;
			if (!first) {
				sb.append(", ");
			} else {
				first = !first;
			}
			sb.append("?");
		}
		return sb.toString();
	}

	private String getColumns(boolean createStatement) {
		StringBuilder sb = new StringBuilder();
		boolean first = true;
		for (TableColumnInfo tableColumnInfo : columns) {
			if (createStatement && !tableColumnInfo.isInsert())
				continue;
			if (!createStatement && !tableColumnInfo.isSelect())
				continue;
			if (!first) {
				sb.append(", ");
			} else {
				first = !first;
			}
			sb.append(tableColumnInfo.getName());
		}
		return sb.toString();
	}

	private String getColumnsUpdate() {
		StringBuilder sb = new StringBuilder();
		boolean first = true;
		for (TableColumnInfo tableColumnInfo : columns) {
			if (!tableColumnInfo.isUpdate())
				continue;
			if (!first) {
				sb.append(", ");
			} else {
				first = !first;
			}
			sb.append(tableColumnInfo.getName() + " = ?");
		}
		return sb.toString();
	}

	public String getPackageName() {
		return packageName;
	}

	public void setPackageName(String packageName) {
		this.packageName = packageName;
	}

	public String getBuildString() {
		return sb.toString();
	}

}
