package allesio.daofx.jdbc.parse;

public class TableColumnInfo {
	private String columnName;
	private String columnTypeName;
	private String columnClassName;

	// if we use this class in different scenarios
	private boolean select;
	private boolean insert;
	private boolean update;

	public TableColumnInfo(String columnName, String columnTypeName, String columnClassName) {
		super();
		this.columnName = columnName;
		this.columnTypeName = columnTypeName;
		this.columnClassName = columnClassName;

		select = true;
		insert = true;
		update = true;
	}

	public String getName() {
		return columnName;
	}

	public void setName(String columnName) {
		this.columnName = columnName;
	}

	public String getColumnTypeName() {
		return columnTypeName;
	}

	public void setColumnTypeName(String columnTypeName) {
		this.columnTypeName = columnTypeName;
	}

	public String getColumnClassName() {
		return columnClassName;
	}

	public void setColumnClassName(String columnClassName) {
		this.columnClassName = columnClassName;
	}

	public boolean isSelect() {
		return select;
	}

	public void setSelect(boolean select) {
		this.select = select;
	}

	public boolean isInsert() {
		return insert;
	}

	public void setInsert(boolean insert) {
		this.insert = insert;
	}

	public boolean isUpdate() {
		return update;
	}

	public void setUpdate(boolean update) {
		this.update = update;
	}

}
