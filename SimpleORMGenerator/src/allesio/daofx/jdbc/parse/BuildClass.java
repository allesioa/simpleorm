package allesio.daofx.jdbc.parse;

import java.sql.SQLException;
import java.util.List;

import allesio.daofx.utils.DBStrings;

public class BuildClass {
	private StringBuilder sb;
	private String packageName;

	public String getBuildString() {
		return sb.toString();
	}

	public void build(String tableName, String optionalClassName, List<TableColumnInfo> columns)
			throws ClassNotFoundException, SQLException {
		sb = new StringBuilder();

		// create java class
		if (null != getPackageName()) {
			sb.append("package " + getPackageName() + ";\n\n");
		}

		sb.append("public class ");
		if (null != optionalClassName) {
			sb.append(optionalClassName);
		} else {
			sb.append(DBStrings.capitalize(tableName));
		}
		sb.append("{ \n");
		createVariables(columns, tableName);
		createGettersSetters(columns);
		sb.append("}");

	}

	private void createGettersSetters(List<TableColumnInfo> columns) {
		if (null == columns || columns.size() == 0)
			return;
		sb.append("\n //Getters/Setters \n");
		for (TableColumnInfo tableColumnInfo : columns) {
			// getter
			sb.append("\t");
			sb.append("public " + tableColumnInfo.getColumnClassName() + " "
					+ DBStrings.toJavaVariable("get_" + tableColumnInfo.getName()) + "(){");
			sb.append("\n\t\t");
			sb.append("return this." + DBStrings.toJavaVariable(tableColumnInfo.getName() + ";"));
			sb.append("\n\t}");
			sb.append("\n");

			// setters
			sb.append("\t");
			sb.append("public void " + DBStrings.toJavaVariable("set_" + tableColumnInfo.getName()) + "("
					+ tableColumnInfo.getColumnClassName() + " " + DBStrings.toJavaVariable(tableColumnInfo.getName())
					+ "){");
			sb.append("\n\t\t");
			sb.append("this." + DBStrings.toJavaVariable(tableColumnInfo.getName()) + " = "
					+ DBStrings.toJavaVariable(tableColumnInfo.getName()) + ";");
			sb.append("\n\t}");
			sb.append("\n");
		}

	}

	private void createVariables(List<TableColumnInfo> columns, String tableName) {
		if (null == columns || columns.size() == 0)
			return;

		for (TableColumnInfo tableColumnInfo : columns) {
			sb.append("\t");
			sb.append("protected " + tableColumnInfo.getColumnClassName() + " "
					+ DBStrings.toJavaVariable(tableColumnInfo.getName()) + ";");
			sb.append("\n");
		}
	}

	public String getPackageName() {
		return packageName;
	}

	public void setPackageName(String packageName) {
		this.packageName = packageName;
	}
}
