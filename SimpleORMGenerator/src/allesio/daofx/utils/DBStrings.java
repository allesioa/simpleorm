package allesio.daofx.utils;

public class DBStrings {
	public static String capitalize(final String string) {
		if (null == string || string.isEmpty()) {
			return string;
		}

		String[] tokens = string.split("\\s");
		StringBuilder sb = new StringBuilder();
		for (String s : tokens) {
			String lowerCase = s.toLowerCase();
			sb.append(Character.toUpperCase(lowerCase.charAt(0)) + lowerCase.substring(1));
		}
		return sb.toString();
	}

	public static String toJavaVariable(final String string) {
		if (null == string || string.isEmpty()) {
			return string;
		}

		char[] charArray = string.toLowerCase().toCharArray();
		StringBuilder sb = new StringBuilder();

		boolean up = false;
		for (char c : charArray) {
			if (c == '_') {
				up = true;
			} else {
				if (up) {
					up = !up;
					c = Character.toUpperCase(c);
				}
				sb.append(c);
			}
		}
		return sb.toString();

	}
}
