package allesio.daofx.ui;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;

import allesio.daofx.jdbc.DBConnector;
import allesio.daofx.jdbc.parse.BuildClass;
import allesio.daofx.jdbc.parse.BuildORM;
import allesio.daofx.jdbc.parse.TableColumnInfo;
import allesio.daofx.ui.saveDlg.SaveDlgController;
import allesio.daofx.utils.DBStrings;
import javafx.application.Platform;
import javafx.beans.binding.Bindings;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.CheckBoxTreeItem;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.PasswordField;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.scene.layout.TilePane;

public class MainAppController {

	@FXML
	private ResourceBundle resources;

	@FXML
	private URL location;

	@FXML
	private TextField urlField;

	@FXML
	private TextField userField;

	@FXML
	private PasswordField passField;

	@FXML
	private TreeView<String> treeView;

	@FXML
	private ToggleGroup select;

	@FXML
	private RadioButton radioAll;

	@FXML
	private RadioButton radioNone;

	@FXML
	private CheckBox cbSave;

	@FXML
	private TilePane content;
	@FXML
	private Button btnConnect;
	@FXML
	private Button btnSaveTables;
	@FXML
	private Button btnSave;
	@FXML
	private ChoiceBox<String> choiceCRUD;

	@FXML
	void connect(ActionEvent event) {
		// if not connected
		if (null == mainAppModel.getConnector()) {
			DBConnector connect = mainAppModel.connect(urlField.getText(), userField.getText(), passField.getText());
			if (null == connect.getConnection())
				return;

			mainAppModel.getProperties().setProperty("url", urlField.getText());
			mainAppModel.getProperties().setProperty("user", userField.getText());
			if (cbSave.isSelected()) {
				mainAppModel.getProperties().setProperty("pass", passField.getText());
			}
			mainAppModel.saveToProperties();

			mainAppModel.getTableList();
			CheckBoxTreeItem<String> setTreeView = mainAppModel.setTreeView(treeView);
			tablesList = setTreeView.getChildren();
			btnSaveTables.setDisable(false);
			urlField.setDisable(true);
			userField.setDisable(true);
			passField.setDisable(true);
			btnConnect.setText("Disconnect");
		} else {
			// disconnect if connected
			DBConnector connector = mainAppModel.getConnector();
			try {
				connector.connectionNULL();
				mainAppModel.setConnector(null);
				tablesList.clear();
				content.getChildren().clear();
			} catch (SQLException e) {
				e.printStackTrace();
			}

			urlField.setDisable(false);
			userField.setDisable(false);
			passField.setDisable(false);
			btnSaveTables.setDisable(true);
			btnConnect.setText("Connect");
		}
	}

	@FXML
	void save(ActionEvent event) {
		// check if only one table is selected
		int selectedTables = 0;
		String tableName = null;
		for (TreeItem<String> item : tablesList) {
			CheckBoxTreeItem<String> cbti = (CheckBoxTreeItem<String>) item;
			if (cbti.isSelected()) {
				if (selectedTables++ > 1) {
					break;
				}
				tableName = cbti.getValue();
			}
		}
		if (selectedTables != 1)
			return;
		// save
		SaveDlgController sDlg = new SaveDlgController();
		sDlg = sDlg.show(mainAppModel, tableName);
		if (sDlg.isSaveAction()) {
			try {
				String fileDestination = sDlg.getTfFileDestination().getText() + File.separator;
				String packageName = sDlg.getTfPackageName().getText();
				String className = sDlg.getTfFileName().getText();
				String ormClassName = sDlg.getTfORMFileName().getText();
				saveToFile(tableName, fileDestination, packageName, className, ormClassName);
			} catch (ClassNotFoundException | SQLException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

	}

	@FXML
	void saveTables(ActionEvent event) { // save
		SaveDlgController sDlg = new SaveDlgController();
		sDlg = sDlg.show(mainAppModel, null);
		if (sDlg.isSaveAction()) {
			for (TreeItem<String> item : tablesList) {
				CheckBoxTreeItem<String> cbti = (CheckBoxTreeItem<String>) item;
				if (cbti.isSelected()) {
					try {
						String fileDestination = sDlg.getTfFileDestination().getText() + File.separator;
						String packageName = sDlg.getTfPackageName().getText();
						String tableName = cbti.getValue();
						String className = DBStrings.capitalize(tableName);
						String ormClassName = className + "ORM";
						loadColumns = mainAppModel.loadColumns(tableName, content, choiceCRUD);
						saveToFile(tableName, fileDestination, packageName, className, ormClassName);
					} catch (ClassNotFoundException | SQLException e) {
						e.printStackTrace();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
		}
	}

	private void saveToFile(String tableName, String fileDestination, String packageName, String className,
			String ormClassName) throws ClassNotFoundException, SQLException, IOException {

		BuildClass buildClass = new BuildClass();
		buildClass.setPackageName(packageName);
		buildClass.build(tableName, className, loadColumns);

		BuildORM buildORM = new BuildORM();
		buildORM.setPackageName(packageName);
		buildORM.build(tableName, ormClassName, className, loadColumns);

		File file = new File(fileDestination + className + ".java");
		file.getParentFile().mkdirs();
		FileWriter fileWriter = new FileWriter(file);
		fileWriter.write(buildClass.getBuildString());
		fileWriter.close();

		File fileOrm = new File(fileDestination + File.separator + "orm" + File.separator + ormClassName + ".java");
		fileOrm.getParentFile().mkdirs();
		FileWriter fileWriterORM = new FileWriter(fileOrm);
		fileWriterORM.write(buildORM.getBuildString());
		fileWriterORM.close();

	}

	private MainAppModel mainAppModel;

	private ObservableList<TreeItem<String>> tablesList;

	private ObservableList<TableColumnInfo> loadColumns;

	public MainAppModel getMainAppModel() {
		return mainAppModel;
	}

	@FXML
	void initialize() {
		btnSaveTables.setDisable(true);
		mainAppModel = new MainAppModel();
		boolean loadFromProperties = mainAppModel.loadFromProperties();
		if (loadFromProperties) {
			urlField.setText(mainAppModel.getProperties().getProperty("url"));
			userField.setText(mainAppModel.getProperties().getProperty("user"));
			String pass = mainAppModel.getProperties().getProperty("pass");
			if (null != pass) {
				cbSave.setSelected(true);
				passField.setText(pass);
			}
			Platform.runLater((() -> btnConnect.requestFocus()));
		}

		select.selectedToggleProperty().addListener((c, oVal, nVal) -> {
			if (null == nVal)
				return;
			if (nVal.equals(radioAll)) {
				for (Node node : content.getChildren()) {
					CheckBox cb = (CheckBox) node;
					cb.setSelected(true);
				}
			} else if (nVal.equals(radioNone)) {
				for (Node node : content.getChildren()) {
					CheckBox cb = (CheckBox) node;
					cb.setSelected(false);
				}
			}
		});

		choiceCRUD.getItems().addAll("SELECT", "INSERT", "UPDATE");
		choiceCRUD.getSelectionModel().selectedItemProperty().addListener((c, oVal, nVal) -> {
			if (null == nVal)
				return;
			for (Node node : content.getChildren()) {
				CheckBox cb = (CheckBox) node;
				TableColumnInfo tci = (TableColumnInfo) cb.getUserData();
				switch (nVal) {
				case "SELECT":
					cb.setSelected(tci.isSelect());
					break;
				case "INSERT":
					cb.setSelected(tci.isInsert());
					break;
				case "UPDATE":
					cb.setSelected(tci.isUpdate());
					break;
				default:
					break;
				}
			}
		});
		treeView.editingItemProperty().addListener((c, oVal, nVal) -> {
			if (null != nVal) {
				CheckBoxTreeItem<String> cb = (CheckBoxTreeItem<String>) nVal;
				if (!cb.isSelected()) {
					loadColumns = mainAppModel.loadColumns(cb.getValue(), content, choiceCRUD);
				}
				choiceCRUD.getSelectionModel().selectFirst();
				cb.setSelected(!cb.isSelected());
			} else {
				CheckBoxTreeItem<String> cb = (CheckBoxTreeItem<String>) oVal;
				cb.setSelected(!cb.isSelected());
				choiceCRUD.getSelectionModel().clearSelection();
				content.getChildren().clear();
			}
		});

		btnSave.disableProperty().bind(Bindings.size(content.getChildren()).isEqualTo(0));
	}
}
