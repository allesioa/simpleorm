package allesio.daofx.ui;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class MainApp extends Application {

	private MainAppController controller;

	@Override
	public void start(Stage primaryStage) throws Exception {
		try {
			FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("Main.fxml"));
			Parent root = fxmlLoader.load();
			controller = fxmlLoader.getController();
			Scene scene = new Scene(root, 800, 600);

			scene.getStylesheets().add(getClass().getResource("Application.css").toExternalForm());
			primaryStage.setScene(scene);
			primaryStage.show();

			primaryStage.onHiddenProperty().addListener((c, oVal, nVal) -> {
				System.out.println("MainApp.start()" + nVal);
				if (null != controller) {
					try {
						controller.getMainAppModel().getConnector().closeConnection();
					} catch (Exception e) {
						e.printStackTrace();
					}
				}

			});

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		launch(args);
	}
}
