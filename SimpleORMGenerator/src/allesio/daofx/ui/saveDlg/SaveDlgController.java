package allesio.daofx.ui.saveDlg;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.Properties;
import java.util.ResourceBundle;

import allesio.daofx.ui.MainAppModel;
import allesio.daofx.utils.DBStrings;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.DirectoryChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.Window;

public class SaveDlgController {

	@FXML
	private ResourceBundle resources;

	@FXML
	private URL location;

	@FXML
	private TextField tfPackageName;

	@FXML
	private TextField tfFileDestination;

	@FXML
	private TextField tfFileName;
	@FXML
	private TextField tfORMFileName;

	@FXML
	private Button btnDirSearch;

	@FXML
	private Button btnSave;

	private Window window;

	private static Stage stage;

	private boolean saveAction;

	private MainAppModel mainAppModel;

	private String tableName;

	@FXML
	void save(ActionEvent event) {
		if (checkFieldValid()) {
			Properties p = mainAppModel.getProperties();
			p.setProperty("dirName", tfFileDestination.getText());
			p.setProperty("packageName", tfPackageName.getText());
			mainAppModel.saveToProperties();
			saveAction = true;
			stage.close();
		}
	}

	@FXML
	void dirSearch(ActionEvent event) {
		DirectoryChooser dc = new DirectoryChooser();

		String path = tfFileDestination.getText().trim();
		if (path.isEmpty()) {
			path = System.getProperty("user.dir");
		}
		dc.setInitialDirectory(new File(path));
		File selectedDir = dc.showDialog(window);
		if (null == selectedDir)
			return;
		String absolutePath = selectedDir.getAbsolutePath();
		tfFileDestination.setText(absolutePath);

		int pos;
		if ((pos = absolutePath.lastIndexOf("src")) > 0) {
			String substring = absolutePath.substring(pos);
			String[] split = substring.split("\\" + File.separator);
			StringBuilder sb = new StringBuilder();
			// leave src out so start at 1
			int length = split.length;
			for (int i = 1; i < length; i++) {
				sb.append(split[i]);
				if (i + 1 < length) {
					sb.append(".");
				}
			}
			tfPackageName.setText(sb.toString());
		}
	}

	@FXML
	void initialize() {
	}

	private void afterInitialize() {
		saveAction = false;

		Properties p = mainAppModel.getProperties();
		String dirName = p.getProperty("dirName");
		if (null != dirName) {
			tfFileDestination.setText(dirName);
		}
		String packageName = p.getProperty("packageName");
		if (null != packageName) {
			tfPackageName.setText(packageName);
		}

		if (null != tableName) {
			tfFileName.setText(DBStrings.capitalize(tableName));
			tfORMFileName.setText(tfFileName.getText() + "ORM");
		} else {
			tfFileName.setDisable(true);
			tfORMFileName.setDisable(true);
		}
	}

	private boolean checkFieldValid() {
		if (tfFileDestination.getText().trim().isEmpty())
			return false;
		File file = new File(tfFileDestination.getText());
		file.getParentFile().mkdirs();
		if (null == file || !file.isDirectory())
			return false;
		if (!tfFileName.isDisabled() && tfFileName.getText().trim().isEmpty())
			return false;
		if (!tfORMFileName.isDisabled() && tfORMFileName.getText().trim().isEmpty())
			return false;
		return true;
	}

	public SaveDlgController show(MainAppModel mainAppModel, String tableName) {
		FXMLLoader loader;
		try {
			loader = new FXMLLoader(getClass().getResource("SaveDlg.fxml"));
			Parent load = loader.load();
			((SaveDlgController) loader.getController()).setMainAppModel(mainAppModel);
			((SaveDlgController) loader.getController()).setTableName(tableName);
			((SaveDlgController) loader.getController()).afterInitialize();
			stage = new Stage();
			stage.setScene(new Scene(load));
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.showAndWait();
			return ((SaveDlgController) loader.getController());
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	private void setTableName(String tableName) {
		this.tableName = tableName;
	}

	public void setMainAppModel(MainAppModel mainAppModel) {
		this.mainAppModel = mainAppModel;
	}

	public TextField getTfFileDestination() {
		return tfFileDestination;
	}

	public TextField getTfPackageName() {
		return tfPackageName;
	}

	public TextField getTfFileName() {
		return tfFileName;
	}

	public TextField getTfORMFileName() {
		return tfORMFileName;
	}

	public boolean isSaveAction() {
		return saveAction;
	}
}
