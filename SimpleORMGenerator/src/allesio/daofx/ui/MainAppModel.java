package allesio.daofx.ui;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;

import allesio.daofx.jdbc.DBConnector;
import allesio.daofx.jdbc.parse.ClassGenerator;
import allesio.daofx.jdbc.parse.TableColumnInfo;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.CheckBox;
import javafx.scene.control.CheckBoxTreeItem;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TreeView;
import javafx.scene.control.cell.CheckBoxTreeCell;
import javafx.scene.layout.TilePane;

public class MainAppModel {
	private DBConnector connector;
	private ObservableList<String> talbeList;
	private Properties props;

	public MainAppModel() {
		talbeList = FXCollections.observableArrayList();
		props = new Properties();
	}

	public DBConnector connect(String url, String user, String pass) {
		try {
			connector = new DBConnector(url, user, pass);
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}
		return connector;
	}

	public DBConnector getConnector() {
		return connector;
	}

	public void setConnector(DBConnector connector) {
		this.connector = connector;
	}

	public void getTableList() {
		if (null == connector)
			return;
		talbeList.clear();

		String sqlGetTables = "select table_name from information_schema.tables WHERE  information_schema.tables.table_schema ='public' ORDER BY table_name";
		ResultSet rs = null;
		try {
			rs = connector.select(sqlGetTables);
			while (rs.next()) {
				talbeList.add(rs.getString(1));
			}

		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				rs.close();
			} catch (SQLException e) {
			}
		}
	}

	public CheckBoxTreeItem<String> setTreeView(TreeView<String> treeView) {
		treeView.setRoot(null);
		CheckBoxTreeItem<String> rootItem = new CheckBoxTreeItem<String>("Tables");
		rootItem.setExpanded(true);
		treeView.setEditable(true);
		treeView.setCellFactory(CheckBoxTreeCell.<String> forTreeView());

		for (String tableName : getTableNames()) {
			CheckBoxTreeItem<String> checkBoxTreeItem = new CheckBoxTreeItem<String>(tableName);
			rootItem.getChildren().add(checkBoxTreeItem);
		}
		treeView.setRoot(rootItem);
		treeView.setShowRoot(true);
		return rootItem;
	}

	public ObservableList<String> getTableNames() {
		return talbeList;
	}

	public Properties getProperties() {
		return props;
	}

	public boolean loadFromProperties() {
		try {
			FileReader reader = new FileReader("lastSession.properties");
			props.load(reader);
			reader.close();
		} catch (FileNotFoundException ex) {
			ex.printStackTrace();
			return false;
		} catch (IOException ex) {
			ex.printStackTrace();
			return false;
		}
		return true;
	}

	public void saveToProperties() {
		try {
			FileWriter writer = new FileWriter("lastSession.properties");
			props.store(writer, null);
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public ObservableList<TableColumnInfo> loadColumns(String tableName, TilePane content, ChoiceBox<String> choice) {
		ClassGenerator classGenerator = new ClassGenerator(getConnector());
		try {
			classGenerator.build(tableName);
			ObservableList<TableColumnInfo> columns = classGenerator.getColumns();
			for (TableColumnInfo tableColumnInfo : columns) {
				CheckBox cb = new CheckBox(tableColumnInfo.getName());
				cb.setUserData(tableColumnInfo);
				cb.selectedProperty().addListener((c, oVal, nVal) -> {
					TableColumnInfo tci = (TableColumnInfo) cb.getUserData();
					switch (choice.getSelectionModel().getSelectedItem()) {
					case "SELECT":
						tci.setSelect(nVal);
						break;
					case "INSERT":
						tci.setInsert(nVal);
						break;
					case "UPDATE":
						tci.setUpdate(nVal);
						break;
					default:
						break;
					}
				});
				content.getChildren().add(cb);

			}
			return columns;
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}
		return null;

	}

}
